package com.example.myapp1.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.myapp1.R;
import com.example.myapp1.fragment.Loginfragment;

public class AuthActivity extends AppCompatActivity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        getSupportActionBar().hide();
        addLoginScreen();
    }

    private void addLoginScreen() {
        Loginfragment loginfragment = new Loginfragment();
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout, loginfragment)
                .commit();
    }
}
