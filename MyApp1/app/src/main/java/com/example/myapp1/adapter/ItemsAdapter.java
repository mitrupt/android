package com.example.myapp1.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.myapp1.R;
import com.example.myapp1.model.Item;
import com.example.myapp1.network.Network;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ItemsAdapter extends RecyclerView.Adapter<ItemsAdapter.ViewHolder> {

    private final Context context;
    private final ArrayList<Item> items;
    @BindView(R.id.imageView)
    ImageView imageView;
    @BindView(R.id.textName)
    TextView textName;
    @BindView(R.id.textPrice)
    TextView textPrice;

    public ItemsAdapter(Context context, ArrayList<Item> items) {
        this.context = context;
        this.items = items;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.recyclerview_item, null));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {

        Item item = items.get(i);

        viewHolder.textName.setText(item.getItem_name());
        viewHolder.textPrice.setText("" + item.getPrice());


        Network.loadImage(context, item.getThumbnail(), viewHolder.imageView);

//        viewHolder.buttonAdd.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                listener.onAdd(i);
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return 0;
    }

    @OnClick(R.id.buttonAdd)
    public void onViewClicked() {
    }

    static
    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imageView)
        ImageView imageView;
        @BindView(R.id.textName)
        TextView textName;
        @BindView(R.id.textPrice)
        TextView textPrice;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    }

