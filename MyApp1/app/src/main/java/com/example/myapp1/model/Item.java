package com.example.myapp1.model;

import com.google.gson.JsonObject;

public class Item {
    private int item_id;
    private String item_name;
    private String thumbnail;
    private float price;

    public static Item parse(JsonObject object){
        Item item = new Item();

        item.item_id = object.get("id").getAsInt();
        item.item_name = object.get("name").getAsString();
        item.thumbnail = object.get("thumbnail").getAsString();
        item.price = object.get("price").getAsFloat();


        return item;

    }

    public Item(){

    }

    public Item(int item_id, String item_name, String thumbnail, float price) {
        this.item_id = item_id;
        this.item_name = item_name;
        this.thumbnail = thumbnail;
        this.price = price;
    }

    @Override
    public String toString() {
        return "Item{" +
                "item_id=" + item_id +
                ", item_name='" + item_name + '\'' +
                ", thumbnail='" + thumbnail + '\'' +
                ", price=" + price +
                '}';
    }

    public int getItem_id() {
        return item_id;
    }

    public void setItem_id(int item_id) {
        this.item_id = item_id;
    }

    public String getItem_name() {
        return item_name;
    }

    public void setItem_name(String item_name) {
        this.item_name = item_name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

}
