package com.example.myapp1.utils;

public class Constants {

    // for APIs
    public static final String SERVER_URL = "http://192.168.43.72:3000/";

    // routes
    public static final String ROUTE_ITEM = "item";
    public static final String ROUTE_USER = "user";
}
