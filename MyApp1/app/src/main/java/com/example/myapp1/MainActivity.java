package com.example.myapp1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.myapp1.fragment.MenuListFragment;

public class MainActivity extends AppCompatActivity {

    MenuListFragment menuListFragment = new MenuListFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initFragment();
    }

    private void initFragment(){
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frameLayout, menuListFragment)
                .commit();
    }
}
